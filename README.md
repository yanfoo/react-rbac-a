# React components and hooks for RBAC-A (ABAC)

This package adds React support for [`@yanfoo/rbac-a`](https://www.npmjs.com/package/@yanfoo/rbac-a).


## Usage

```js
import React, { useRef } from 'react';
import RBACReact, { usePermission, JsonRBACProvider } from '@yanfoo/react-rbac-a';

import { useAuthentication } from './hooks/useAuthentication';

import permissionData from './permission-data.json';


const LayoutComponent = () => {
   // will have the same properties as useRef, except that the check
   // method won't be user bound
   const rbacRef = useRef();

   return (
      <RBACReact ref={ rbacRef } provider={ new JsonRBACProvider(permissionData) }>
         <SecureComponent />
      </RbacProvider>
   );
);

const SecureComponent = () => {
   const { user } = useAuthentication();
   const { ready, granted } = usePermission(user, 'common:view.secureComponent');

   return (
      <div>
         { !reader ? (
            <div>...checking permission</div>
         ) : granted ? (
            <div>User can view this component!</div>
         ) : (
            <div>User is restricted for this component</div>
         ) }
      </div>
   );
}
```


## API

```js
import RBACReact, { 
   useRBAC,
   usePermission,
   
   
   // exported from @yanfoo/rbac-a
   RBAC,
   RBACAttribute,
   RBACProvider,
   JsonRBACProvider,
} from '@yanfoo/react-rbac-a';


const provider = ...;
const attributes = ...;
const options = ...;
const context = ...;  // e.g. { foo: true }

const PermissionProvider = children => 
   <RBACReact 
      ready={ true }
      provider={ provider } 
      attributes={ attributes } 
      options={ options }
      context={ context }
   >
      { children }
   </RbacProvider>
;

const SecureComponent = (permissions, children) => {
   const { ready, granted } = usePermission(user, permissions, options);

   return ready && granted ? children : null;
};

const CustomSecureComponent = children => {
   const rbac = useRBAC(user);
   // NOTE : any properties defined in "context" will be accessible (e.g. rbac.foo = true)
   
   useEffect(() => {

      // rbac.ready equals <RBACReact ready={ ... }> (defaults to true)
      if (rbac.ready) {
         rbac.check(permissions, options).then(granted => {
            // ...
         });
      }

   }, [rbac]);

   return children;
}
```


**NOTE** : both `user`, `permissions`, and `options` are described in [`@yanfoo/rbac-a`](https://www.npmjs.com/package/@yanfoo/rbac-a)'s documentation.



## Contribution

All contributions welcome! Every PR **must** be accompanied by their associated unit tests!


## License

The MIT License (MIT)

Copyright (c) 2015 Mind2Soft <yanick.rochon@mind2soft.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
