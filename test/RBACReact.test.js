import React, { Component, useEffect, useRef, useState } from 'react'
import { render, screen, act } from '@testing-library/react'
import RBACReact, { usePermissions, usePermissionsList } from '../src/index';
import { RBACProvider } from '@yanfoo/rbac-a';



//https://kentcdodds.com/blog/common-mistakes-with-react-testing-library

describe('Testing RBACReact', () => {

   class MockProvider extends RBACProvider {
      async getRoles(user) {
         return [
            { role: 'test', level:1, permissions: [{ can:'valid_1' }] },
            { role: 'test', level:2, permissions: [{ can:'valid_2' }] }
         ];
      }
   }


   class TestContainerNoProvider extends Component {
      static getDerivedStateFromError(error) {
         return { hasError: true, error };
      }

      constructor() {
         super();

         this.state = {
            hasError: false,
            error: null
         };
      }

      componentDidCatch(error, errorInfo) {
         //console.error(error, errorInfo);
         this.setState(prevState => ({ ...prevState, error, hasError: true }));
      }
      
      render() {
         return this.state.hasError ? (
            <div data-testid="test">{ this.state.error.message }</div>      
         ) : this.props.children 
      }
   };

   class TestContainer extends Component {
      static getDerivedStateFromError(error) {
         return { hasError: true, error };
      }

      constructor() {
         super();

         this.state = {
            hasError: false,
            error: null,
            provider: new MockProvider()
         };
      }

      componentDidCatch(error, errorInfo) {
         //console.error(error, errorInfo);
         this.setState(prevState => ({ ...prevState, error, hasError: true }));
      }
      
      render() {
         return (
            <RBACReact ready={ this.props.ready } provider={ this.state.provider }>
               { this.state.hasError ? (
                  <div data-testid="test">{ this.state.error.message }</div>      
               ) : this.props.children }
            </RBACReact>
         );
      }
   };

   const UsePermissionsComponent = ({ user, permissions }) => {
      const { ready, granted } = usePermissions(user, permissions);
      
      const className = granted ? 'allowed' : 'denied';
      const content = ready ? granted ? 'allowed' : 'denied' : 'loading';

      return (
         <div data-testid="output" className={ className }>{ content }</div>
      );
   }

   const UsePermissionsListComponent = ({ user, permissionsList }) => {
      const { ready, granted } = usePermissionsList(user, permissionsList);
      
      const className = granted.length ? granted.map(allowed => allowed ? 'allowed' : 'denied').join(' ') : 'denied';
      const content = ready ? granted.map(allowed => allowed ? `allowed-${allowed}`: 'denied').join(',') : 'loading';

      return (
         <div data-testid="output" className={ className }>{ content }</div>
      );
   }


   describe('usePermissions', () => {

      it('should be loading when provider is not ready', async () => {
         await act(async () => {
            render(<TestContainer ready={ false }><UsePermissionsComponent user="1" permissions="invalid_1" /></TestContainer>);
         });

         expect(screen.getByTestId('output')).toHaveTextContent('loading');
         expect(screen.getByTestId('output')).toHaveClass('denied');
      });

      it('should fail when no provider specified', async () => {
         const __error = console.error;
         console.error = () => {};

         await act(async () => {
            render(<TestContainerNoProvider><UsePermissionsComponent user="1" permissions="valid_1" /></TestContainerNoProvider>);
         });

         expect(screen.getByTestId('test')).toHaveTextContent('Missing RBACReact provider');

         console.error = __error;
      });

      it('should allow permission', async () => {
         await act(async () => {
            render(<TestContainer ready={ true }><UsePermissionsComponent user="1" permissions="valid_1" /></TestContainer>);
         });

         expect(screen.getByTestId('output')).toHaveTextContent('allowed');
      });


      it('should deny permission', async () => {
         await act(async () => {
            render(<TestContainer ready={ true }><UsePermissionsComponent user="1" permissions="invalid_1" /></TestContainer>);
         });

         expect(screen.getByTestId('output')).toHaveTextContent('denied');
      });

   });


   describe('usePermissionsList', () => {

      it('should be loading when provider is not ready', async () => {
         const permissionsList = ['invalid_1', 'invalid_2'];

         await act(async () => {
            render(<TestContainer ready={ false }><UsePermissionsListComponent user="1" permissionsList={ permissionsList } /></TestContainer>);
         });

         expect(screen.getByTestId('output')).toHaveTextContent('loading');
         expect(screen.getByTestId('output')).toHaveClass('denied');
      });

      it('should fail when no provider specified', async () => {
         const permissionsList = ['valid_1', 'valid_2'];
         const __error = console.error;
         console.error = () => {};

         await act(async () => {
            render(<TestContainerNoProvider><UsePermissionsListComponent user="1" permissionsList={ permissionsList } /></TestContainerNoProvider>);
         });

         expect(screen.getByTestId('test')).toHaveTextContent('Missing RBACReact provider');

         console.error = __error;
      });

      it('should allow permission', async () => {
         const permissionsList = ['valid_1', 'valid_2'];

         await act(async () => {
            render(<TestContainer ready={ true }><UsePermissionsListComponent user="1" permissionsList={ permissionsList } /></TestContainer>);
         });

         expect(screen.getByTestId('output')).toHaveTextContent('allowed-1,allowed-2');
      });


      it('should deny permission', async () => {
         const permissionsList = ['valid_1', 'invalid_1'];

         await act(async () => {
            render(<TestContainer ready={ true }><UsePermissionsListComponent user="1" permissionsList={ permissionsList } /></TestContainer>);
         });

         expect(screen.getByTestId('output')).toHaveTextContent('allowed-1,denied');
      });

   });


   describe('Functional components', () => {

      const TestComponent = () => {
         const rbacRef = useRef();
         const [ hasValidRef, setValidRef ] = useState(false);

         useEffect(() => {
            setValidRef(!!rbacRef?.current?.check);
         }, []);

         return (
            <RBACReact ref={ rbacRef } ready={ true } provider={ new MockProvider() }>
               <div data-testid="output" className={ hasValidRef ? 'validRef' : 'invalid' }></div>
            </RBACReact>
         );
      }


      it('should have valid ref', async () => {

         await act(async () => {
            render(<TestComponent />);
         });
   
         expect(screen.getByTestId('output')).toHaveClass('validRef');
   

      });


   });


});
