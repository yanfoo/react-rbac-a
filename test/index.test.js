import RBACReact, { useRBAC, usePermissions } from '../src/index';


describe('Testing entry point', () => {

   it('should expose provider and hooks', () => {
      expect(RBACReact).toBeInstanceOf(Object);
      expect(RBACReact.render).toBeInstanceOf(Function);
      expect(RBACReact.propTypes).toHaveProperty('provider');
      expect(RBACReact.propTypes).toHaveProperty('attributes');
      expect(RBACReact.propTypes).toHaveProperty('options');
      expect(RBACReact.propTypes).toHaveProperty('children');

      expect(useRBAC).toBeInstanceOf(Function);
      expect(usePermissions).toBeInstanceOf(Function);      
   });

});