import React, { createContext, forwardRef, useContext, useEffect, useImperativeHandle, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import RBAC, { JsonRBACProvider, RBACProvider, RBACAttribute } from '@yanfoo/rbac-a';


const RBACReactContext = createContext({
   ready: true,
   check: () => { throw new Error('Missing RBACReact provider'); }
});

const RBACReact = forwardRef(({ 
   ready,
   provider,
   attributes,
   options,
   context,
   children
}, ref) => {
   const contextValue = useMemo(() => {
      const rbac = new RBAC({ provider, attributes, options });
      return {
         ...context,
         __rbac: rbac,
         check: (user, permissions, options) => rbac.check(user, permissions, options),
         ready: !!ready
      };
   }, [ ready, context, provider, attributes, options ])

   useImperativeHandle(ref, () => contextValue, [contextValue]);
   
   return (
      <RBACReactContext.Provider value={ contextValue }>{ children }</RBACReactContext.Provider>
   );
});

RBACReact.propTypes = {
   ready: PropTypes.bool,
   provider: PropTypes.instanceOf(RBACProvider).isRequired,
   attributes: PropTypes.oneOfType([PropTypes.object, PropTypes.instanceOf(Map)]),
   options: PropTypes.object,
   children: PropTypes.element.isRequired
};

RBACReact.defaultProps = {
   ready: true
};



const useRBAC = user => {
   const rbac = useContext(RBACReactContext);
   
   return useMemo(() => {
      const { check, ...context } = rbac;

      return {
         ...context,
         check: (permissions, options) => rbac.check(user, permissions, options),
      };
   }, [user, rbac]);
};


const usePermissions = (user, permissions, options) => {
   const rbac = useRBAC(user);
   const [{ ready, granted }, setPermission] = useState({ ready:true, granted:false });

   useEffect(() => {
      let active = true;
      
      if (rbac.ready) {
         setPermission(({ granted }) => ({ ready:false, granted }));
         rbac.check(permissions, options).then(level => active && setPermission({ ready:true, granted:!isNaN(level) ? level : false }));
      } else {
         setPermission({ ready:false, granted:false });
      }

      return () => active = false;
   }, [ permissions, options, rbac ]);

   return { ready, granted:ready && granted };
};


const usePermissionsList = (user, permissionsList, options) => {
   const rbac = useRBAC(user);
   const [{ ready, granted }, setPermission] = useState({ ready:true, granted:[] });

   useEffect(() => {
      let active = true;
      
      if (rbac.ready && Array.isArray(permissionsList)) {
         setPermission(({ granted }) => ({ ready:false, granted }));

         Promise.all(permissionsList.map(permissions => rbac.check(permissions, options).then(level => !isNaN(level) ? level : false))).then(granted => {
            active && setPermission({ ready:true, granted });
         });
      } else {
         setPermission({ ready:false, granted:[] });
      }

      return () => active = false;
   }, [ permissionsList, options, rbac ]);

   return { ready, granted:ready ? granted : [] };
};



export default RBACReact;
export { 
   useRBAC,
   usePermissions,
   usePermissionsList,
   RBAC,
   RBACAttribute,
   RBACProvider,
   JsonRBACProvider
};